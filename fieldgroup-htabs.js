(function ($) {

var idKey = 'fieldgroup_htabs_id';
var fieldgroup_htabs_counter = 0;
var panesClass = 'horizontal-tabs-panes';
var panesHTML = '<div class="' + panesClass + '"></div>';

/**
 * This behavior adapts fieldgroup_htabs markup for use with the
 * horizontal-tabs.js script.
 */
Drupal.behaviors.fieldgroup_htabs = function (context) {
  var tabs = [], groups = [], tabGroups = [];
  var lastTab = null, lastGroup = null, staticGroup = null;
  var staticTabs = $('div.fieldgroup-htabs-static-group').length > 0;

  // Do a preliminary check for static group.
  if (staticTabs) {
    staticGroup = 0;
    groups[staticGroup] = $('div.fieldgroup-htabs-static-group').addClass('field-group-htabs-wrapper htabs').append($(panesHTML))
      .find('.' + panesClass);
    tabGroups[staticGroup] = [];
  }

  $('fieldset.fieldgroup-htabs:not(.fieldgroup-htabs-processed)', context).addClass('fieldgroup-htabs-processed')
  // Change .fieldset-content to .fieldset-wrapper
  .find('.fieldset-content')
    .removeClass('fieldset-content')
    .addClass('fieldset-wrapper')
  .end()
  // Tag each fieldgroup with a unique id.
  .each(function () {
    var id = fieldgroup_htabs_counter ++;
    tabs[id] = $(this);
    tabs[id].data(idKey, id);
  })
  // Group the fieldgroups into separate tab groups.
  .each(function () {
    var thisId = $(this).data(idKey);
    var groupToAdd = null;

    // Check if this is a static tab.
    if (staticTabs && $(this).is('.fieldgroup-htabs-static-tab')) {
      groupToAdd = staticGroup;
    }
    // Check if this is adjacent to the last tab.
    else if (lastTab !== null && tabs[lastTab].next('fieldset.fieldgroup-htabs').data(idKey) === thisId) {
      groupToAdd = lastGroup;
    }
    // No tab group determined. Create new tab group.
    // This means that this is the first non-static fieldgroup or this is not
    // adjacent to the last tab.
    else {
      // Prepare next group id.
      if (lastGroup === null) {
        // Create the first non-static tab group.
        lastGroup = tabGroups.length;
      }
      else {
        lastGroup ++;
      }

      // Add group.
      groups[lastGroup] = $('<div class="field-group-htabs-wrapper htabs"></div>').append($(panesHTML))
        .insertBefore($(this))
        .find('.' + panesClass);
      tabGroups[lastGroup] = [];

      groupToAdd = lastGroup;
    }

    // Add fieldgroup to tab group.
    tabGroups[groupToAdd].push(thisId);

    // Prepare for next fieldgroup.
    lastTab = thisId;
  });

  // Add tabs into groups.
  for (var gId = 0; gId < tabGroups.length; gId ++) {
    var tabGroup = tabGroups[gId];
    for (var tId = 0; tId < tabGroup.length; tId ++) {
      groups[gId].append(tabs[tabGroup[tId]]);
    }
  }
}

})(jQuery);